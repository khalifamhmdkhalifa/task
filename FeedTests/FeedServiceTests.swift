//
//  FeedServiceTests.swift
//  FeedTests
//
//  Created by Sergey Shavrin on 14/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import XCTest

@testable import Feed

class FeedServiceTests: XCTestCase {
  
  private let service = FeedService()
  
  func testBefore() {
    let date = self.service.dateFormatter.date(from: "01:00:30")!
    let exp = self.expectation(description: "Didn't receive values")

    service.getOlderItems(before: date, limit: 5) { items in
      XCTAssertTrue(items.count == 5)
      XCTAssertEqual(items[0].title, "01:00:29")
      XCTAssertEqual(items[1].title, "01:00:28")
      XCTAssertEqual(items[2].title, "01:00:27")
      XCTAssertEqual(items[3].title, "01:00:26")
      XCTAssertEqual(items[4].title, "01:00:25")

      exp.fulfill()
    }

    self.waitForExpectations(timeout: 3, handler: nil)
  }

  func testAfter() {
    let date = self.service.dateFormatter.date(from: "01:00:30")!
    let exp = self.expectation(description: "Didn't receive values")

    service.getNewerItems(after: date, limit: 5) { items in
      XCTAssertTrue(items.count == 5)
      XCTAssertEqual(items[0].title, "01:00:35")
      XCTAssertEqual(items[1].title, "01:00:34")
      XCTAssertEqual(items[2].title, "01:00:33")
      XCTAssertEqual(items[3].title, "01:00:32")
      XCTAssertEqual(items[4].title, "01:00:31")
      exp.fulfill()
    }

    self.waitForExpectations(timeout: 3, handler: nil)
  }
  
  func testNow() {
    let date = Date().addingTimeInterval(-2.0)
    let exp = self.expectation(description: "Didn't receive values")

    service.getNewerItems(after: date, limit: 5) { items in
      XCTAssertTrue(items.count == 2)
      exp.fulfill()
    }

    self.waitForExpectations(timeout: 3, handler: nil)
  }
}
