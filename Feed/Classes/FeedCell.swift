//
//  FeedCell.swift
//  Feed
//
//  Created by Sergey Shavrin on 13/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import UIKit

final class FeedCell: UITableViewCell {
  
  static let reuseIdentifier = "FeedCellReuseIdentifier"
  
  public var title: String? {
    get { return self.titleLabel.text }
    set { self.titleLabel.text = newValue }
  }
  
  private let titleLabel = UILabel(frame: .zero)
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.setupLayout()
    self.setupUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.title = "..."
  }
  
  // MARK: Private Methods
  
  private func setupLayout() {
    self.contentView.addSubview(self.titleLabel)
    
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      self.titleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
      self.titleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
      self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 24),
      self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -24)
    ])
  }
  
  private func setupUI() {
    self.titleLabel.font = UIFont.systemFont(ofSize: 32, weight: .bold)
    self.titleLabel.textAlignment = .center
    self.selectionStyle = .none
  }
  
}
