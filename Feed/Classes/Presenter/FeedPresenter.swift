//
//  FeedPresenter.swift
//  Feed
//
//  Created by khalifa on 5/31/21.
//  Copyright © 2021 Glovo. All rights reserved.
//

import Foundation

protocol FeedView: class {
    func showItems(_ items: [FeedItem])
}


class FeedPresenter {
    private weak var view: FeedView?
    private var items: [FeedItem] = []
    private var firstItemLoaded: FeedItem?
    private var isLoading: Bool = false
    private let maxLoadedItems = 50
    private var loadingPrevious = false
    private var loadingNext = false

    init(view: FeedView) {
        self.view = view
    }
}

extension FeedPresenter: FeedViewOutput {
    func onViewDidLoad() {
        FeedService().getLatestItems { [weak self] items in
          self?.items = items
          self?.view?.showItems(items)
        }
    }
    
    func onViewNeedsToPrefetch(items: [Int]) {
        let sortedItems = items.sorted()
        guard let firstIndex = sortedItems.first,
              let lastIndex = sortedItems.last else { return }
        if firstIndex  <= 1 {
            loadPreviousPageIfNeeded()
        } else if lastIndex >= sortedItems.count-3 {
            loadNextPageIfNeeded()
        }
    }
    
    private func loadNextPageIfNeeded() {
        guard !loadingNext, let lastItem = items.last else { return }
        loadingNext = true
        print("STARTED TO LOAD")
        FeedService().getOlderItems(before: lastItem.createdAt) {[weak self] result in
            guard let self = self else { return }
            self.loadingNext = false
            self.items.append(contentsOf: self.items)
            if self.items.count > self.maxLoadedItems {
            self.items.removeFirst(result.count)
            }
            self.view?.showItems(self.items)
        }
    }
    
    private func loadPreviousPageIfNeeded() {
        guard let firstItem = items.first, !loadingPrevious,
              firstItem != self.firstItemLoaded else { return }
        loadingPrevious = true
        FeedService().getNewerItems(after: firstItem.createdAt) {[weak self] result in
            guard let self = self else { return }
            self.loadingPrevious = false
            self.items = result + self.items
            if self.items.count > self.maxLoadedItems {
            self.items.removeLast(result.count)
            }
            self.view?.showItems(self.items)
        }
    }
}
