//
//  FeedItem.swift
//  Feed
//
//  Created by Sergey Shavrin on 13/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import Foundation

final class FeedItem {
  let createdAt: Date
  let title: String
  
  init(createdAt: Date, title: String) {
    self.createdAt = createdAt
    self.title = title
  }
  
}

extension FeedItem: Equatable {
    static func == (lhs: FeedItem, rhs: FeedItem) -> Bool {
        return lhs.createdAt == rhs.createdAt
    }
}
