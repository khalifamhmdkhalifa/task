//
//  FeedService.swift
//  Feed
//
//  Created by Sergey Shavrin on 13/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import Foundation

final class FeedService {
  
  let dateFormatter = DateFormatter()
  
  init() {
    self.dateFormatter.dateFormat = "hh:mm:ss"
  }
  
  func getNewerItems(after: Date, limit: Int = 10, callback: @escaping ([FeedItem]) -> Void) {
    self.getItems(date: after, range: 1 ... limit, callback: callback)
  }
  
  func getOlderItems(before: Date, limit: Int = 10, callback: @escaping ([FeedItem]) -> Void) {
    self.getItems(date: before, range: -limit ... -1, callback: callback)
  }
  
  func getLatestItems(limit: Int = 10, callback: @escaping ([FeedItem]) -> Void) {
    self.getOlderItems(before: Date(), limit: limit, callback: callback)
  }
  
  // MARK: Private Methods
  
  private func getItems(date: Date, range: ClosedRange<Int>, callback: @escaping ([FeedItem]) -> Void) {
    var items: [FeedItem] = []
    
    for i in range.reversed() {
      let createdAt = date.addingTimeInterval(Double(i))
      
      if createdAt.timeIntervalSinceNow > 0 {
        continue
      }
      
      let title = self.dateFormatter.string(from: createdAt)
      items.append(FeedItem(createdAt: createdAt, title: title))
    }

    let delay = Double.random(in: 0.25...2.00)
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
      callback(items)
    }
  }
  
}
