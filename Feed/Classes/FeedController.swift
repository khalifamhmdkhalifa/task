//
//  FeedController.swift
//  Feed
//
//  Created by Sergey Shavrin on 13/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import UIKit

protocol FeedViewOutput {
    func onViewNeedsToPrefetch(items: [Int])
    func onViewDidLoad()
}

final class FeedController: UITableViewController {
  
  private var items: [FeedItem] = []
  private var presenter: FeedViewOutput?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.prefetchDataSource = self
    presenter = FeedPresenter(view: self)
    self.presenter?.onViewDidLoad()
    self.tableView.register(FeedCell.self, forCellReuseIdentifier: FeedCell.reuseIdentifier)
    self.tableView.tableFooterView = UIView()
    self.presenter?.onViewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
   
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.items.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.reuseIdentifier) as! FeedCell
    cell.title = self.items[indexPath.row].title
    return cell
  }

}


extension FeedController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        presenter?.onViewNeedsToPrefetch(items: indexPaths.map({$0.item}))
    }
}
extension FeedController: FeedView {
    func showItems(_ items: [FeedItem]) {
        self.items = items
        tableView.reloadData()
    }
    
    
}
