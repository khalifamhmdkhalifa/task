//
//  AppDelegate.swift
//  Feed
//
//  Created by Sergey Shavrin on 13/03/2019.
//  Copyright © 2019 Glovo. All rights reserved.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    -> Bool
  {
    self.window = UIWindow(frame: UIScreen.main.bounds)
    self.window?.rootViewController = FeedController()
    self.window?.makeKeyAndVisible()
    
    return true
  }

}
